// productos.ts

import { ModuleResolutionKind } from "typescript";

// Creando una lista de productos
interface Producto { 
    nombre: string; 
    precio: number;
    stock: number; 
  } 
  //
  const Producto1: Producto = { 
    nombre: "Yogurt", 
    precio: 100,
    stock: 13
  }; 
  const Producto2: Producto = { 
   nombre: "Paquete de Galletas", 
   precio: 89.9,
   stock: 40
 };
 const Producto3: Producto = { 
   nombre: "Lata Coca-Cola", 
   precio: 80.50,
   stock: 0
 };
 const Producto4: Producto = {
   nombre: "Alfajor",
   stock: 12,
   precio: 40
 }
 
 
 let productos:Array<Producto> = [
   Producto1,
   Producto2,
   Producto3,
   Producto4
 ]
 //module.exports = productos;
 //console.log(productos);
 
export function getStock () {
    return productos;

   //for (let i = 0; i < productos.length; i++) {
   //  console.log(`Lo sentimos, no hay mas unidades del Producto ${productos[i].nombre}`);      
   //  if( productos[i].stock === 0 ){
   // }else{
   //    console.log(`Del Producto ${productos[i].nombre} quedan ${productos[i].stock} unidades`);    
   //  }    
   //}
 }
 //module.exports = getStock;
// getStock(productos) 

export function storeProductos(body:any)
{
  productos.push({
   nombre: body.nombre,
   stock: body.stock,
   precio: body.precio
  })
}

export function deleteProductos(indice:number)
 {
   productos.splice(indice,1);
 }

 export function actualizarProductos(body:any, indice:number){
  productos.splice(indice,1,{
    nombre: body.nombre,
   stock: body.stock,
   precio: body.precio
  })
 }
