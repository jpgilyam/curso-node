// Importamos el paquete express

const express = require('express');
//const productos = require('./productos');
const app = express();
app.use(express.json());
import * as productos from './productos'




//import express from 'express'


//import * as productos from './productos'


// Instanciamos una app de express
//const app = express()


// Definimos una ruta y su handler correspondiente
app.get('/', function (request: any, response: { send: (arg0: string) => void; }) {
    response.send('¡Bienvenidos a Express!')
})


// Ponemos a escuchar nuestra app de express

app.listen(3000, function () {
  console.info('Servidor escuchando en http://localhost:3000')
})


//console.log(productos.getStock() );

app.get('/productos',function(request: any, response: { send: (arg0: any) => void; }) {
    response.send( productos.getStock()  );
    
})



app.post('/productos', function(request: any, response: { send: (arg0: string) => void; }) {
    console.log("se va a gregar un producto, stock actual");
    console.log(productos.getStock());
    
    
    
    productos.storeProductos(request.body);
    console.log("se agrego un produto, esta es la nueva lista");
    console.log(productos.getStock());
    response.send( JSON.stringify(productos.getStock()  ));
})


app.delete(`/productos/:id` , function(request: any, response: { send: (arg0: string) => void; }) {
    console.log(`Stock actual, se va a eliminar el producto ${request.params.id}`);    
    console.log(productos.getStock());
    productos.deleteProductos(request.params.id);
    console.log(`se elimino el producto ${request.params.id}`);    
    console.log(productos.getStock());
    response.send(`se elimino el producto ${request.params.id}`);    
}) 


app.put('/productos/:id',function(request: any, response: { send: (arg0: string) => void; }){
    console.log(`se va a actualizar el producto ${request.params.id}`); 
    console.log(productos.getStock());
    productos.actualizarProductos(request.body,request.params.id);
    console.log(`se actualizo el parametro ${request.params.id}`); 
    console.log(productos.getStock());
    response.send(`se actualizo el parametro ${request.params.id}`);    
})
